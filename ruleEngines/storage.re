acPostProcForParallelTransferReceived(*leaf_resource) {
    msiWriteRodsLog("LOGGING: acPostProcForParallelTransferReceived", *Status);
    msi_update_unixfilesystem_resource_free_space(*leaf_resource);
}

acPostProcForDataCopyReceived(*leaf_resource) {
    msiWriteRodsLog("LOGGING: acPostProcForDataCopyReceived", *Status);
    msi_update_unixfilesystem_resource_free_space(*leaf_resource);
}

# for iput
acPostProcForPut {
    msi_update_unixfilesystem_resource_free_space($KVPairs.rescName);
}

acPostProcForPut { }

# for storage update after irmtrash
acPostProcForDelete {
    msi_update_unixfilesystem_resource_free_space($KVPairs.rescName);
}

acPostProcForDelete { }
