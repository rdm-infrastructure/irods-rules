deleteHiddenFiles{
  *user = $userNameClient
  foreach(*row in SELECT COLL_NAME, DATA_NAME, DATA_OWNER_NAME where DATA_NAME like "%.DS_Store"){
    *coll = *row.COLL_NAME;
    *obj = *row.DATA_NAME;
    *objpath = *coll++"/"++*obj;
    *owner = *row.DATA_OWNER_NAME;

    writeLine("serverLog", "DEBUG: DELETE hidden file: *objpath");
    if(*owner == *user){
        #expects the user to call the rule when it should be executed.
        #scheduling will not work due to the stdout
        writeLine("stdout", "DEBUG: DELETE hidden file: *objpath");
        msiDataObjUnlink( "objPath=*objpath++++forceFlag=" , *status);
        writeLine("stdout", "DEBUG: DataObj Unlink status *status");
    }
    else{
        writeLine("serverLog", "DEBUG: *user, *objpath");
        if(*user == "rods"){
            #the user rods must have rodsadmin rights
            #all output will be written to the serverLog file
            msiSetACL("default", "admin:own", "*user", "*objpath");
            msiDataObjUnlink( "objPath=*objpath++++forceFlag=" , *status);
            writeLine("serverLog", "DEBUG: DataObj Unlink status *status");
        }
        else{
            #in the case that a user has no rights and cannot claim the rights to 
            #delete the file communicatin will be written to the serverlog
            #if the rule is not called interactively but scheduled, 
            #the second line will crash
            writeLine("serverLog", "DEBUG: No permission for *user to delete *objpath.");
            writeLine("stdout", "DEBUG: No permission to delete *objpath.");
        }
    }
  }
  msiDeleteUnusedAVUs();
}
