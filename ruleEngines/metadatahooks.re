######################################################
# Metadata policies
#
# The tag "RESOURCE" is reserved for data objects only.
# It can only take the values:
#     disk, archive, dual
#
# The rules below replicate the data according to the
# resource tag:
#     disk: demoResc
#     archive: 500Gvault
#     dual: disk and archive
# During the replication the data is always updated 
# to the latest copy.
#
# Author: Christine Staiger (2021)
#######################################################

# Policy when metadata is newly created on data objects
# add, adda, addw, set
acPreProcForModifyAVUMetadata(
    *Option,*ItemType,*ItemName,*AName,*AValue,*AUnit){
    on(*AName == "RESOURCE" && (*AValue == 'archive' || *AValue == 'disk' || *AValue == 'dual')
                            && *ItemType == "-d" && *Option!="rm" && *Option!="rmw" && *Option!="rmi"){
        writeLine("serverLog", "PREPROCESSING: CREATE METADATA");
        writeLine("serverLog",
            "DEBUG *Option,*ItemType,*ItemName,*AName,*AValue,*AUnit");
        rescPolicy(*ItemName, *AValue);
    }
}

acPreProcForModifyAVUMetadata(
    *Option,*ItemType,*ItemName,*AName,*AValue,*AUnit){
    on(*AName != "RESOURCE" || *Option == "rm" || *Option == "rmw" || *Option == "rmi"){}
}

# Policy when metadata is modified on data objects
acPreProcForModifyAVUMetadata(
    *Option,*ItemType,*ItemName,*AName,*AValue,*AUnit, *NAName, *NAValue, *NAUnit){
    on(*AName == "RESOURCE" && (*AValue == 'archive' || *AValue == 'disk' || *AValue == 'dual')
                            && *ItemType == "-d"){
        writeLine("serverLog", "PREPROCESSING: MODIFY METADATA");
        writeLine("serverLog",
            "DEBUG *Option,*ItemType,*ItemName,*AName,*AValue,*AUnit,*NAName,*NAValue,*NAUnit");
        *newval = substr(*NAValue, 2, strlen(*NAValue));
        writeLine("serverLog", *newval);
        rescPolicy(*ItemName, *newval);
    }
}

acPreProcForModifyAVUMetadata(
    *Option,*ItemType,*ItemName,*AName,*AValue,*AUnit, *NAName, *NAValue, *NAUnit){
    on(*AName != "RESOURCE"){}
}

# Policy for copying metadata --> doing nothing for the moment
#acPreProcForModifyAVUMetadata(
    #*Option,*SourceItemType,*TargetItemType,*SourceItemName,*TargetItemName){ 
    #writeLine("serverLog", "PREPROCESSING: COPY METADATA");
    #writeLine("serverLog",
    #    "DEBUG *Option,*SourceItemType,*TargetItemType,*SourceItemName,*TargetItemName")
#}

# General reosurce policy for data objects
rescPolicy(*abspath, *value){
    on(*value == "archive"){
        *archive = "500Gvault";
        replicate(*abspath, *archive);
        foreach(*row in select RESC_NAME where RESC_NAME != *archive){
            *resc = *row.RESC_NAME;
            writeLine("serverLog", "DEBUG msiDataObjTrim(*abspath, *resc, null, 1, null, state)");
            errormsg(msiDataObjTrim(*abspath, *resc, "null", "1", "null", *state), *err);
            writeLine("serverLog", "DEBUG TRIM: state *state, trimming error message --> *err");
        }
    }
}

rescPolicy(*abspath, *value){
    on(*value == "disk" || *value == ""){
        *disk = "demoResc";
        replicate(*abspath, *disk);
        foreach(*row in select RESC_NAME where RESC_NAME != *disk){
            *resc = *row.RESC_NAME;
            writeLine("serverLog", "DEBUG msiDataObjTrim(*abspath, *resc, null, 1, null, state)");
            errormsg(msiDataObjTrim(*abspath, *resc, "null", "1", "null", *state), *err);
            writeLine("serverLog", "DEBUG TRIM: state *state, trimming error message --> *err");
        }
    }
}

rescPolicy(*abspath, *value){
    on(*value == "dual"){
        *archive = "500Gvault";
        *disk = "demoResc";
        replicate(*abspath, *archive);
        replicate(*abspath, *disk);
    }
}


replicate(*abspath, *rescname){
    writeLine("serverLog", "DEBUG REPLICATE: *abspath");
    errormsg(msiDataObjRepl(*abspath, "destRescName=*rescname++++updateRepl=++++verifyChksum=", *state), *err);
    writeLine("serverLog", "DEBUG REPLICATE: state *state, repl error message --> *err");
}

