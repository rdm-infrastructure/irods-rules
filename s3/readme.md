# Extra S3 rulesets

### Authors
Christine Staiger
Joris Luijsterburg


Wageningen University & Research 2022

Based on irods 4.2.8 running on ubuntu 18.04

This is a set of rules that you can use in combination with an s3 resource. There is a number of files in the repo:

### Prerequisites
These rules use icommands(iadmin) and the s3cmd tool.

For iadmin this means that you need to have your ~/.irods/environment.json file, and that the user is rodsadmin. When running the interactive user this will mean you, when running in the ruleengine this is apparantly alreayd guaranteed by the irods system.

For s3cmd you can google for installation instructions. However, you will need to hav a .s3cfg file on your home directory. For the interactive rule this would mean in your own directory, for the ruleengine it would mean /var/lib/irods/.s3cfg. An example configfile is below:

```
# Setup endpoint
host_base = 10.0.0.10:9000 
host_bucket = 10.0.0.10:9000

bucket_location = us-east-1
use_https = False

# Setup access keys minio res
access_key = key_given_by_s3_administrator
secret_key = secret_given_by_s3_administrator

# Enable S3 v4 signature APIs
signature_v2 = False

```


### msiExecCmd_bin Folder
These files need to be installed in the /var/lib/irods/msiExecCmd_bin folder

### interactiveRules folder
These files can be installed on any folder of your liking, and can be used to execute the rules interactively

### ruleEngines
These files need to be installed in the /etc/irods folder. Also you need to change the /etc/irods/server_config.json to look for these files. Below is an example on how to do this. I only added line 13.

```
 1        "rule_engines": [
 2            {
 3                "instance_name": "irods_rule_engine_plugin-irods_rule_language-instance",
 4                "plugin_name": "irods_rule_engine_plugin-irods_rule_language",
 5                "plugin_specific_configuration": {
 6                    "re_data_variable_mapping_set": [
 7                        "core"
 8                    ],
 9                    "re_function_name_mapping_set": [
10                        "core"
11                    ],
12                    "re_rulebase_set": [
13                        "user_rules",
14                        "core"

```


