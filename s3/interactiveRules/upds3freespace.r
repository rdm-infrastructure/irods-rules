upds3freespace{
 #first test if this is an s3resource or not, and get the bucketname at the same time
  foreach(*resource in SELECT RESC_TYPE_NAME,RESC_VAULT_PATH where RESC_NAME = "*resc"){
    *resourcetype=*resource.RESC_TYPE_NAME
    *bucket=*resource.RESC_VAULT_PATH
    if(*resourcetype!="s3"){
      writeLine("stdout","This is not an s3 resource, the rule doesn't work with a non s3-resource")
      failmsg(-1,"This is not an s3 resource, the rule doesn't work with a non s3-resource")
    }
  }

  #get bucket free space in variable
  writeLine("stdout","fetching disk usage of irods bucket with maximum of 10MB");
  msiExecCmd("s3diskusagefromresc","*bucket 10240", "null", "null", "null", *OUTPUT);

  #TODO: check if output is correct, the resource could be ill configured
  #msiGetStderrInExecCmdOut(*OUTPUT,*ErrorOut);
  #writeLine("stdout","Error message is *ErrorOut");

  #printing messages
  msiGetStdoutInExecCmdOut(*OUTPUT,*Out);
  writeLine("stdout","Output message is *Out");



  #TODO: update freespace with icommand iadmin

}
INPUT *resc="s3resc2"
OUTPUT ruleExecOut
