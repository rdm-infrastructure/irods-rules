acPostProcForParallelTransferReceived(*leaf_resource){
  # triggered when?
  msiWriteRodsLog("LOGGING:acPostProcForParallelTransferRecieved", *Status);
  WUR_User_Update_Free_Space_In_Resource($KVPairs.rescName);
  msiWriteRodsLog("LOGGING:acPostProcForPut: updating filesystem succesfull!!!", *Status);

}

acPostProcForDataCopyReceived(*leaf_resource){
  #triggered when?
  msiWriteRodsLog("LOGGING:acPostProcForDataCopyRecieved", *Status);
  WUR_User_Update_Free_Space_In_Resource($KVPairs.rescName);
  msiWriteRodsLog("LOGGING:acPostProcForPut: updating filesystem succesfull!!!", *Status);
}


acPostProcForPut {
  # triggered after a put
  #msi_update_unixfilesystem_resource_free_space($KVPairs.rescName);
  msiWriteRodsLog("LOGGING:acPostProcForPut", *Status);
  WUR_User_Update_Free_Space_In_Resource($KVPairs.rescName);
  msiWriteRodsLog("LOGGING:acPostProcForPut: updating filesystem succesfull!!!", *Status);
}


acPostProcForDelete {
  #msi_update_unixfilesystem_resource_free_space($KVPairs.rescName);
  msiWriteRodsLog("LOGGING:acPostProcForDelete", *Status);
  WUR_User_Update_Free_Space_In_Resource($KVPairs.rescName);
  msiWriteRodsLog("LOGGING:acPostProcForPut: updating filesystem succesfull!!!", *Status);
}


acPostProcForPut {}
acPostProcForDelete {}
acPostProcForParallelTransferReceived(*leaf_resource) {}
acPostProcForDataCopyReceived(*leaf_resource) {}



#Helperfunctions

WUR_User_Check_Resource_type(*resc,*resourcetype){
  msiWriteRodsLog("LOGGING: WUR_User_Check_Resource_type: checking resource type for resource *resc", *Status);
  foreach(*resource in SELECT RESC_TYPE_NAME where RESC_NAME = "*resc"){
     *resourcetype=*resource.RESC_TYPE_NAME
     msiWriteRodsLog("LOGGING: WUR_User_Check_Resource_type: resource type of resource *resc is *resourcetype", *Status);
     *returnvalue="*resourcetype";
  }
}

WUR_User_Update_Free_Space_In_Resource(*leaf_rsc) {
  WUR_User_Check_Resource_type(*leaf_rsc,*resourcetype)
  msiWriteRodsLog("LOGGING: WUR_User_Update_Free_Space_In_Resource: resource type of *leaf_rsc is *resourcetype", *Status);
  if("*resourcetype" == "unixfilesystem"){
    msiWriteRodsLog("LOGGING: WUR_User_Update_Free_Space_In_Resource: updating unixfilesystem", *Status);
    msi_update_unixfilesystem_resource_free_space(*leaf_rsc);
  }
  else{
    if(*resourcetype == "s3"){
      msiWriteRodsLog("LOGGING:WUR_User_Update_Free_Space_In_Resource: updating s3 free space in resource *leaf_rsc", *Status);
      WUR_User_Update_S3_Resource_Free_Space(*leaf_rsc)
    }
    else{
      msiWriteRodsLog("LOGGING: WUR_User_Update_Free_Space_In_Resource: No freespaceupdate defined for this resource of type *resourcetype for resource *leaf_rsc", *Status);
    }
  }
}

WUR_User_Update_S3_Resource_Free_Space(*resc){
 #TODO: total space in bucket is now hardcoded at 10MB. Not sure where to set this yet
 #TODO: harden it with more errorhandling in rule and bash scripts


 #first test if this is an s3resource or not, and get the bucketname at the same time
  foreach(*resource in SELECT RESC_TYPE_NAME,RESC_VAULT_PATH where RESC_NAME = "*resc"){
    *resourcetype=*resource.RESC_TYPE_NAME
    *bucket=*resource.RESC_VAULT_PATH
    if(*resourcetype!="s3"){
      msiWriteRodsLog("LOGGING: WUR_User_Update_S3_Resource_Free_Space: This is not an s3 resource, the rule doesn't work with a non s3-resource",*Status);
      failmsg(-1,"This is not an s3 resource, the rule doesn't work with a non s3-resource")
    }
  }

  #get bucket free space in variable
  msiWriteRodsLog("LOGGING: WUR_User_Update_S3_Resource_Free_Space: fetching disk usage of irods bucket with maximum of 10MB",*Status);
  msiExecCmd("s3diskusagefromresc","*bucket 10240", "null", "null", "null", *OUTPUT);

  #TODO: check if output is correct, the resource could be ill configured
  #msiGetStderrInExecCmdOut(*OUTPUT,*ErrorOut);
  #writeLine("stdout","Error message is *ErrorOut");

  #printing messages
  msiGetStdoutInExecCmdOut(*OUTPUT,*kbfreeinresc);
  msiWriteRodsLog("LOGGING: WUR_User_Update_S3_Resource_Free_Space: Free space in *resc is *kbfreeinresc  kb",*Status);

  #updating actual free space
  msiExecCmd("changerescfreespace","*resc *kbfreeinresc", "null", "null", "null", *OUTPUT2);
}

