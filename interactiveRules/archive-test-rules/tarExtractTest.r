tarExtractTest{
        msiSplitPath(*obj, *parentColl, *objName);
        msiCollCreate("*parentColl/unpack", 1, *collCreateOut);
        msiCollCreate("*parentColl/unpackOne", 1, *collCreateOut);
        msiArchiveExtract(*obj, "*parentColl/unpackOne", "test.txt", *resource, *outTarExtract);
        msiArchiveExtract(*obj, "*parentColl/unpack", "null", *resource, *outTarExtract);
        writeLine("stdout", "Extracted to *parentColl/unpack");
}

input *obj=" path to tar or zip", *resource="demoResc"
output ruleExecOut
