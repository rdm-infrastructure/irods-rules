# Replicates data to another resource 
# (+1 copies of the bitstream afterwards, not the data object!).
# USAGE:
# irule -F replicateData.r "*coll='/npecZone/home/christine/game'" "*resc='demoResc'"
# irule -F replicateData.r "*coll='/npecZone/home/christine/game'" "*data='test.txt'" "*resc='demoResc'"

replicateData{
    #debug check input
    writeLine("stdout", "*coll, *data, *resc");
    msiGetObjType(*coll, *colltype);
    msiGetObjType(*resc, *resctype);

    if(*resctype != '-r'){
        writeLine("stdout", "No resource specified.");
        msiGoodFailure()
    }
    if(*colltype != "-c"){
        writeLine("stdout", "No valid collection given.")
        msiGoodFailure()
    }
    #single file
    if(*data != "" ){
        msiGetObjType("*coll/*data", *datatype);
        if(*datatype != "-d"){
            writeLine("stdout", "No valid data given.");
            msiGoodFailure()
        }
        else{
            replicate(*coll, *data, *resc);
        }
    }
    #collection
    else{
        writeLine("stdout", "Replicating collection.")
        foreach(*row in SELECT DATA_NAME, COLL_NAME
                where COLL_NAME like "*coll/%"){
            *dataname = *row.DATA_NAME;
            *collname = *row.COLL_NAME;
            writeLine("stdout", "*collname, *dataname");
            replicate(*collname, *dataname, *resc);
        }
        foreach(*row in SELECT DATA_NAME, COLL_NAME
                where COLL_NAME like *coll){
            *dataname = *row.DATA_NAME;
            *collname = *row.COLL_NAME;
            writeLine("stdout", "*collname, *dataname");
            replicate(*collname, *dataname, *resc);
        }
    }
}

replicate(*collname, *dataname, *rescname){
    writeLine("stdout", "Replication: *collname, *dataname");
    *repl = true;
    foreach(*row in SELECT RESC_NAME
            where DATA_NAME like *dataname AND COLL_NAME like *collname){
        writeLine("stdout", *row.RESC_NAME);
        if(*row.RESC_NAME == *rescname){*repl = false}
    }
    if(*repl){
        msiDataObjRepl("*collname/*dataname", "destRescName=*rescname++++verifyChksum=",
                       *state)}
    else{writeLine("stdout", "No replication, data already on resource.")}
}

INPUT *coll = "", *data = "", *resc = ""
OUTPUT ruleExecOut
