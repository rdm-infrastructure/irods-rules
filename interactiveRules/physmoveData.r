# Moves the bitstream of a data object to another resource.
# USAGE: 
# irule -F physmoveData.r "*coll='/npecZone/home/christine/game'" "*resc='demoResc'"
# irule -F physmoveData.r "*coll='/npecZone/home/christine/game'" "*data='test.txt'" "*resc='demoResc'"

replicateData{
    #debug check input
    writeLine("stdout", "*coll, *data, *resc");
    msiGetObjType(*coll, *colltype);
    msiGetObjType(*resc, *resctype);

    if(*resctype != '-r'){
        writeLine("stdout", "No resource specified.");
        msiGoodFailure()
    }
    if(*colltype != "-c"){
        writeLine("stdout", "No valid collection given.")
        msiGoodFailure()
    }
    #single file
    if(*data != "" ){
        msiGetObjType("*coll/*data", *datatype);
        if(*datatype != "-d"){
            writeLine("stdout", "No valid data given.");
            msiGoodFailure()
        }
        else{
            physmove(*coll, *data, *resc);
        }
    }
    #collection
    else{
        writeLine("stdout", "Trimming collection.")
        foreach(*row in SELECT DATA_NAME, COLL_NAME
                where COLL_NAME like "*coll/%"){
            *dataname = *row.DATA_NAME;
            *collname = *row.COLL_NAME;
            writeLine("stdout", "*collname, *dataname");
            physmove(*collname, *dataname, *resc);
        }
        foreach(*row in SELECT DATA_NAME, COLL_NAME
                where COLL_NAME like *coll){
            *dataname = *row.DATA_NAME;
            *collname = *row.COLL_NAME;
            writeLine("stdout", "*collname, *dataname");
            physmove(*collname, *dataname, *resc);
        }
    }
}

physmove(*collname, *dataname, *rescname){
    writeLine("stdout", "Moving *collname/*dataname to resource *rescname");
    msiDataObjPhymv("*collname/*dataname", *rescname, "null", "null", "null", *state);
}

INPUT *coll = "", *data = "", *resc = ""
OUTPUT ruleExecOut
