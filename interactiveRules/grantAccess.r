grantAccess{
    *abspath = "/$rodsZoneClient/home/$userNameClient/*path"
	writeLine("stdout", "Granting *acl access to *abspath for *group");
    msiSetACL("recursive", *acl, *group, *abspath)
}

INPUT *path="testdata",*group="technicians",*acl="read"
OUTPUT ruleExecOut
