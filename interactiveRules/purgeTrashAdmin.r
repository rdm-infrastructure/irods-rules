purgeTrashAdmin{
    on(userType($userNameClient)=="rodsadmin"){
        if(*grace == 0){*grace = 30 * 24 * 60 * 60}
        msiGetSystemTime(*now, *status);
        *cutoff = int(*now)-*grace;
        *abspath = "/$rodsZoneClient/trash"
        writeLine("serverLog",
                  "PURGETRASH: Delete all trashed files older than *cutoff");
        purgeTrash(*cutoff, $userNameClient);
    }
}

userType(*user){
    foreach(*row in SELECT USER_TYPE where USER_NAME like "*user"){
        *type=*row.USER_TYPE;
    }
    *type; #return value
}


purgeTrash(*cutoff, *user){
    # Purges the trash of all users
    *abspath = "/$rodsZoneClient/trash"
    foreach(*row in SELECT COLL_NAME, DATA_NAME, DATA_MODIFY_TIME where COLL_NAME like "*abspath/%"){
        *objAccessTime = int(*row.DATA_MODIFY_TIME);
        *coll = *row.COLL_NAME;
        *obj = *row.DATA_NAME;
        *path = "*coll/*obj";
        if(int(*cutoff) > int(*objAccessTime)){
            writeLine("serverLog", "PURGETRASH: Permanently deleting *path");
            msiSetACL("default", "admin:own", "*user", "*path");
            msiDataObjUnlink("*path++++forceFlag=", *status);
        }
        writeLine("serverLog","Check *coll");
        deleteEmptyColl(*coll);
    }

    msiDeleteUnusedAVUs();
}

deleteEmptyColl(*abspath){
    # Deletes a collection tree if no data objects are stored there.
    foreach(*row in SELECT count(DATA_NAME) where COLL_NAME like "*abspath%"){
        *count = *row.DATA_NAME;
    }
    if(int(*count) == int(0)){
        writeLine("serverLog", "*abspath: DELETE");
        msiRmColl(*abspath, "irodsAdminRmTrash=++++forceFlag=", *status);
        msiDeleteUnusedAVUs();
    }
}

INPUT *grace=0
OUTPUT ruleExecOut

