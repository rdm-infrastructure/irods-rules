# Policy that creates and removes the bitstreams of data objects from and on resources.
# This is driven by a metadata key value pair.
# RESOURCE: archive --> 500Gvault
# RESOURCE: disk --> demoResc
# RESOURCE: dual --> 500Gvault and demoResc
# script assumes 1! entry with the keyword "RESOURCE".

# to be implemented in acPostProcForModifyAVUmetadata

storagePolicy{
    #*archive = "500Gvault";
    #*disk = "demoResc";

    #debug check input
    writeLine("stdout", "*coll, *data");
    msiGetObjType(*coll, *colltype);
    if(*colltype != "-c"){
        writeLine("stdout", "ERROR: No valid collection given.");
        msiGoodFailure()
    }

    if(*data != ""){
        msiGetObjType("*coll/*data", *datatype);
        if(*datatype != "-d"){
            writeLine("stdout", "ERROR: No valid data given.");
            msiGoodFailure()
        }
        # function here
        *value = getMetadataValue("RESOURCE", *coll, *data);
        writeLine("stdout", "DEBUG: *coll/*data with RESOURCE: *value.");
        rescPolicy(*coll, *data, *value);
    }
    else{
        foreach(*row in select DATA_NAME where COLL_NAME like *coll){
            *dataname = *row.DATA_NAME;
            *value = getMetadataValue("RESOURCE", *coll, *dataname);
            writeLine("stdout", "DEBUG: *coll/*dataname with RESOURCE: *value.");
            rescPolicy(*coll, *dataname, *value);
        }
        foreach(*row in select DATA_NAME, COLL_NAME where COLL_NAME like "*coll/%"){
            *dataname = *row.DATA_NAME;
            *collname = *row.COLL_NAME;
            *value = getMetadataValue("RESOURCE", *coll, *dataname);
            writeLine("stdout", "DEBUG: *collname/*dataname with RESOURCE: *value.");
            rescPolicy(*collname, *dataname, *value);
        }
    }
}

getMetadataValue(*key, *collname, *dataname){
    *keyavail = false;
    foreach(*row in select META_DATA_ATTR_NAME where
            COLL_NAME like *collname and DATA_NAME like *dataname){
        if(*row.META_DATA_ATTR_NAME == *key){
            *keyavail = true
        }
    }
    if(*keyavail){
        foreach(*row in select META_DATA_ATTR_VALUE where
                META_DATA_ATTR_NAME like *key and
                COLL_NAME like *collname and DATA_NAME like *dataname){
            *value = *row.META_DATA_ATTR_VALUE;
            writeLine("stdout", "DEBUG: Value found *value.")
        }
    }
    else{ *value = ""}
    *value
}

rescPolicy(*collname, *dataname, *value){
    on(*value == "archive"){
        replicate(*collname, *dataname, *archive);
        foreach(*row in select RESC_NAME where
                RESC_NAME != *archive and COLL_NAME like *collname and
                DATA_NAME like *dataname){
            *rescname = *row.RESC_NAME;
            trim(*collname, *dataname, *rescname);
        }
    }
}

rescPolicy(*collname, *dataname, *value){
    on(*value == "disk" || *value == ""){
        writeLine("stdout", "DEBUG: disk or empty; *collname/*dataname");
        replicate(*collname, *dataname, *disk);
        foreach(*row in select RESC_NAME where
                RESC_NAME != *disk and COLL_NAME like *collname and
                DATA_NAME like *dataname){
            *rescname = *row.RESC_NAME;
            trim(*collname, *dataname, *rescname);
        }
    }
}

rescPolicy(*collname, *dataname, *value){
    on(*value == "dual"){
        replicate(*collname, *dataname, *archive);
        replicate(*collname, *dataname, *disk);
    }
}

replicate(*collname, *dataname, *rescname){
    writeLine("stdout", "Replication: *collname, *dataname");
    *repl = true;
    foreach(*row in SELECT RESC_NAME
            where DATA_NAME like *dataname AND COLL_NAME like *collname){
        writeLine("stdout", *row.RESC_NAME);
        if(*row.RESC_NAME == *rescname){*repl = false}
    }
    if(*repl){
        msiDataObjRepl("*collname/*dataname", "destRescName=*rescname++++verifyChksum=",
                       *state)}
    else{writeLine("stdout",
                   "DEBUG: No replication, *collname/*dataname already on *rescname.")}
}

trim(*collname, *dataname, *rescname){
    writeLine("stdout", "Trimming *collname/*dataname from *rescname");
    foreach(*row in SELECT count(RESC_NAME) where
            DATA_NAME like *dataname AND COLL_NAME like *collname){
            *numResc = *row.RESC_NAME;
    }
    if(int(*numResc) > 1){
        #check if one replica is on resc
        foreach(*row in SELECT RESC_NAME
                where DATA_NAME like *dataname AND COLL_NAME like *collname){
            *r = *row.RESC_NAME;
            if(*r == *rescname){
                msiDataObjTrim("*collname/*dataname", *rescname, "null", "1", "null", *state);
            }
        }
    }
    else{writeLine("stdout",
        "DEBUG: No trimming, Only 1 copy of *collname/*dataname in the system.")}
}
INPUT *coll = "/unlock/home/christine.staiger@wur.nl", *data = "", *archive = "archiveResc", *disk = "dataResc"
OUTPUT ruleExecOut

