# Trimming replicas of data objects.
# The rule makes sure that there is always at least one copy of the data in the system.
# USAGE: 
# irule -F trimData.r "*coll='/npecZone/home/christine/game'" "*resc='demoResc'"
# irule -F trimData.r "*coll='/npecZone/home/christine/game'" "*data='test.txt'" "*resc='demoResc'"

replicateData{
    #debug check input
    writeLine("stdout", "*coll, *data, *resc");
    msiGetObjType(*coll, *colltype);
    msiGetObjType(*resc, *resctype);

    if(*resctype != '-r'){
        writeLine("stdout", "No resource specified.");
        msiGoodFailure()
    }
    if(*colltype != "-c"){
        writeLine("stdout", "No valid collection given.")
        msiGoodFailure()
    }
    #single file
    if(*data != "" ){
        msiGetObjType("*coll/*data", *datatype);
        if(*datatype != "-d"){
            writeLine("stdout", "No valid data given.");
            msiGoodFailure()
        }
        else{
            trim(*coll, *data, *resc);
        }
    }
    #collection
    else{
        writeLine("stdout", "Trimming collection.")
        foreach(*row in SELECT DATA_NAME, COLL_NAME
                where COLL_NAME like "*coll/%"){
            *dataname = *row.DATA_NAME;
            *collname = *row.COLL_NAME;
            writeLine("stdout", "*collname, *dataname");
            trim(*collname, *dataname, *resc);
        }
        foreach(*row in SELECT DATA_NAME, COLL_NAME
                where COLL_NAME like *coll){
            *dataname = *row.DATA_NAME;
            *collname = *row.COLL_NAME;
            writeLine("stdout", "*collname, *dataname");
            trim(*collname, *dataname, *resc);
        }
    }
}

trim(*collname, *dataname, *rescname){
    writeLine("stdout", "Trimming *collname/*dataname from *rescname");
    foreach(*row in SELECT count(RESC_NAME) where
            DATA_NAME like *dataname AND COLL_NAME like *collname){
            *numResc = *row.RESC_NAME;
    }
    if(int(*numResc) > 1){
        #check if one replica is on resc
        foreach(*row in SELECT RESC_NAME
                where DATA_NAME like *dataname AND COLL_NAME like *collname){
            *r = *row.RESC_NAME;
            if(*r == *rescname){
                writeLine("stdout", "Trimming");
                msiDataObjTrim("*collname/*dataname", *rescname, "null", "1", "null", *state);
            }
        }
    }
    else{writeLine("stdout", "Only 1 copy of data in the system.")}
}

INPUT *coll = "", *data = "", *resc = ""
OUTPUT ruleExecOut
