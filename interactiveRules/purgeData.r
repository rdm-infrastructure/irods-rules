purgeData{
    # Purges all data objects in *abspath that are older than *grace. 
    # Data will NOT be placed in trash.
    # Defaults:
    #   *abspath - User's trash
    #   *grace   - 1 month (in seconds)
    # Usage: irule -F purgeData.r "*abspath='/npecZone/home/christine/game1'" "*grace=7*24*60*60"
    #        irule -F purgeData.r "*grace=1" # Delete everything in trash 

    if(*abspath == ""){*abspath = "/$rodsZoneClient/trash/home/$userNameClient"}

    msiGetSystemTime(*now, *status);
    if(*grace == 0){*grace = 30 * 24 * 60 * 60}
    *diff = int(*now)-*grace;

    foreach(*row in SELECT COLL_NAME, DATA_NAME, DATA_MODIFY_TIME where COLL_NAME like "*abspath%") {
        *objAccessTime = int(*row.DATA_MODIFY_TIME);
        *coll = *row.COLL_NAME;
        *obj = *row.DATA_NAME;
        *path = "*coll/*obj";
        #writeLine("stdout", "-- *path; *diff > *objAccessTime");
        if(int(*diff) > int(*objAccessTime)){
            writeLine("stdout", "Last access: *objAccessTime; Delete: *path");
            msiDataObjUnlink("*path++++forceFlag=", *status);
        }
    }
    msiDeleteUnusedAVUs();
}

INPUT *abspath = "", *grace=0
OUTPUT ruleExecOut
