# Example rules

## Synopsis

In this document we take you through example rules to create and update EPICPIDs. All PIDs are resolvable through:

```
http://hdl.handle.net/
```

e.g.:

```
http://hdl.handle.net/<prefix>/<suffix>
http://hdl.handle.net/<prefix>/<suffix>?noredirect
```

## Create EPIC PIDs

- **Create** PID for data object or collection

  ```
  irule -F registerData.r "*path='/bobZone/home/bob/test.txt'"
  DEBUG RegisterData: Create EPIC PID for /bobZone/home/bob/test.txt.
  DEBUG RegisterData: Created 21.T12995/381cb850-dd7d-11eb-a862-00505684a77c for /bobZone/home/bob/test.txt
  DEBUG RegisterData: EPIC PID lookup for /bobZone/home/bob/test.txt: 21.T12995/381cb850-dd7d-11eb-a862-00505684a77c.
  ```

  The rule will create an EPIC PID with the following profile:

  ![PID-bob](figs/PID-bob.png)

- **Resolving** of PIDs

  Note, that iRODS paths are not actionable through HTTP(s). If you want an actionable URL you can direct the PID URL field to the davrods endpoint by setting the variable `*proto` accordingly:

  ```
  irule -F registerData.r "*path='/bobZone/home/bob/test.txt'" "*proto='https://scomp1446.wur.nl'"
  ```

  The resolving of the EPIC PID is now redirected to https://scomp1446.wur.nl/bobZone/home/bob/test.txt.

- **Recursive** creation of PIDs

  If you want to create PIDs for a collection recursively set the `*recursive` variable to true:

  ```
   irule -F registerData.r "*path='/bobZone/home/bob/registered'" "*proto='https://scomp1446.wurnet.nl'" "*recursive='true'"
  ```

## Update EPIC PIDs

- **Moving** registered data objects in iRODS

  When data which carries a PID is moved, the iRODS path changes . Hence we need to update the URL field in the EPIC PID.

  Assume we move a registered data object from:

  ```
  imeta ls -d /bobZone/home/bob/test.txt
  AVUs defined for dataObj /bobZone/home/bob/test.txt:
  attribute: EPIC_PID
  value: 21.T12995/616ec698-dd7e-11eb-9382-00505684a77c
  units:
  ```

  to:

  ```
  imv test.txt my-test.txt
  ```

  The URL in the EPIC PID would now no longer point to the correct data object. However, we have the PID in the metadata of iRODS and can adjust the fields `URL` and `IRODS_PATH`:

  ```
  irule -F updateEpicUrl.r "*path='/bobZone/home/bob/my-test.txt'"
  ```

  Note, that also here you can change the endpoint with the variable `*proto`.

  Just like in the step to create persistent identifiers, you can also use the recursive flag to update the EPIC PIDs of all members of a collection:

  ```
  irule -F updateEpicUrl.r "*path='/bobZone/home/bob/published'" "*recursive='true'"
  ```

## Removing data and updating their PIDs accordingly

Once a PID is created it should always exist (*persistent*) even if the data the PID points to is removed. However, once the data ceased to exist, PIDs will no longer resolve nor automatically carry the information that the data is no longer there.

So, before a data object or a collection is removed in iRODS, we need to adjust the PID. Here we will point the URL to `http://hdl.handle.net/<PREFIX/SUFFIX>?noredirect` to ensure that the PID still resolves to some meaningful state information; and we will introduce a new key `DATA_STATE` with the value `deleted` in the EPIC PID's metadata.

```
irule -F deleteRegData.r "*path='/bobZone/home/bob/published'" "*recursive='true'"
DEBUG RegisterData: Create EPIC PID for /bobZone/home/bob/published and all its members.
INFO deleteRegData Ready for deletion: /bobZone/home/bob/published/test1.txt
INFO deleteRegData Ready for deletion: /bobZone/home/bob/published/test2.txt
INFO deleteRegData Ready for deletion: /bobZone/home/bob/published/test3.txt
INFO deleteRegData Ready for deletion: /bobZone/home/bob/published/test4.txt
INFO deleteRegData Ready for deletion: /bobZone/home/bob/published/subcoll/test5.txt
INFO deleteRegData Ready for deletion: /bobZone/home/bob/published/subcoll
INFO deleteRegData Ready for deletion: /bobZone/home/bob/published
```

Metadata in the EPIC PID after calling the iRODS rule:



![PID-deletedData](figs/PID-deletedData.png)

Now we can safely remove the data from iRODS.

```
irm -r /bobZone/home/bob/published
```
