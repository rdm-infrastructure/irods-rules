deleteRegData{
    # Before data can be safely deleted in iRODS, adjust the EPIC PID metadata.
    # If the input path is a collection and the recursive flag is set to true 
    # all members of the collection will receive an individual EPIC PID.
    #
    # *path: full path do object or collection
    # *recursive: apply to all memebers of a collection, if=gnored if path is an object
    
    msiGetObjType(*path, *objType);
    *epic = isregistered(*path, *objType);
    if(*epic != ""){
        if(*objType == "-c" && *recursive == "true"){
            writeLine("stdout","DEBUG RegisterData: Create EPIC PID for *path and all its members.");
            foreach(*row in SELECT COLL_NAME, DATA_NAME where COLL_NAME like *path){
		*coll = *row.COLL_NAME;
	        *obj = *row.DATA_NAME;
	        *epic = isregistered("*coll/*obj", "-d");
		if(*epic != ""){
		    updatePIDprofile(*epic);
		    writeLine("stdout", "INFO deleteRegData Ready for deletion: *coll/*obj")
		}
	    }
	    foreach(*row in SELECT COLL_NAME, DATA_NAME where COLL_NAME like "*path/%"){
                *coll = *row.COLL_NAME;
                *obj = *row.DATA_NAME;
                *epic = isregistered("*coll/*obj", "-d");
                if(*epic != ""){
		    updatePIDprofile(*epic);
		    writeLine("stdout", "INFO deleteRegData Ready for deletion: *coll/*obj")
		}
            }
	    foreach(*row in SELECT COLL_NAME where COLL_NAME like "*path/%"){
                *coll = *row.COLL_NAME;
                *epic = isregistered("*coll", "-c");
                if(*epic != ""){
		    updatePIDprofile(*epic);
		    writeLine("stdout", "INFO deleteRegData Ready for deletion: *coll")
		}
            }
            if(*epic != ""){
                updatePIDprofile(*epic);
	        writeLine("stdout", "INFO deleteRegData Ready for deletion: *path")
	    }
        }
        else{
	    updatePIDprofile(*epic);
            writeLine("stdout", "INFO deleteRegData Ready for deletion: *path")
        }
    }
}

#Helper functions
updatePIDprofile(*epic){
    msiUpdateEpicPID(*epic, "URL", "http://hdl.handle.net/*epic?noredirect", *out);
    msiUpdateEpicPID(*epic, "DATA_STATE", "deleted", *out);
}

isregistered(*path, *objType){
    # Checks if EPIC PID for *path is known in the iRODS metadata, key=EPIC_PID.
    # Returns the EPIC PID or "".
    *epic = "";
    if(*objType == "-d"){
        msiSplitPath(*path, *coll, *obj);
        foreach(*row in SELECT META_DATA_ATTR_VALUE where META_DATA_ATTR_NAME = 'EPIC_PID' and COLL_NAME = '*coll' and DATA_NAME = '*obj'){ 
            *epic = *row.META_DATA_ATTR_VALUE;
	}
    }
    if(*objType == '-c'){
        foreach(*row in SELECT META_COLL_ATTR_VALUE where META_COLL_ATTR_NAME == "EPIC_PID" and COLL_NAME == "*path"){
            *epic = *row.META_COLL_ATTR_VALUE;
	}
    }
    *epic;
} 

input *path='/<zone>/home/<user>/<obj or coll>',*recursive='false'
output ruleExecOut
