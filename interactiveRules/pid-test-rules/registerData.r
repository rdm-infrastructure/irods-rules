registerData{
    # Creates a PID for *path and saves the PID in the iRODS metadata key=EPIC_PID, value=Handle
    # If the input path is a collection and the recursive flag is set to true 
    # all members of the collection will receive an individual EPIC PID.
    #
    # *path: full path do object or collection
    # *recursive: apply to all memebers of a collection, if=gnored if path is an object
    
    msiGetObjType(*path, *objType);
    if(*objType == "-c" && *recursive == "true"){
        writeLine("stdout","DEBUG RegisterData: Create EPIC PID for *path and all its members.");
        # register all data objects in the collection
        foreach(*row in SELECT COLL_NAME, DATA_NAME where COLL_NAME like "*path"){
	    *coll = *row.COLL_NAME;
	    *obj = *row.DATA_NAME;
	    *epic = isregistered("*coll/*obj", "-d");
	    if(*epic == ""){
	        writeLine("stdout","DEBUG RegisterData: Create EPIC PID for *coll/*obj.");
                register("*coll/*obj", *proto, "-d");
                *epic = isregistered("*coll/*obj", "-d");

	    }
            writeLine("stdout",
                      "DEBUG RegisterData: EPIC PID lookup for *coll/*obj: *epic.");

	}
        # register all data objects in subcollections
	foreach(*row in SELECT COLL_NAME, DATA_NAME where COLL_NAME like "*path/%"){
            *coll = *row.COLL_NAME;
            *obj = *row.DATA_NAME;
            *epic = isregistered("*coll/*obj", "-d");
            if(*epic == ""){
                writeLine("stdout","DEBUG RegisterData: Create EPIC PID for *coll/*obj.");
                register("*coll/*obj", *proto, "-d");
		*epic = isregistered("*coll/*obj", "-d");
            }
            writeLine("stdout",
                      "DEBUG RegisterData: EPIC PID lookup for *coll/*obj: *epic.");

        }
	# register all subcollections
        foreach(*row in SELECT COLL_NAME where COLL_NAME like "*path/%"){
            *coll = *row.COLL_NAME;
            *epic = isregistered(*coll, "-c");
            if(*epic == ""){
                writeLine("stdout","DEBUG RegisterData: Create EPIC PID for *coll.");
                register(*coll, *proto, "-c");
                *epic = isregistered(*coll, "-c");
            }
	    writeLine("stdout",
                      "DEBUG RegisterData: EPIC PID lookup for *coll/*obj: *epic.");
        }
	# register the collection itself
	*epic = isregistered(*path, *objType);
        if(*epic == ""){
            writeLine("stdout","DEBUG RegisterData: Create EPIC PID for *path.");
            register(*path, *proto, *objType);
	    *epic = isregistered(*path, *objType);
        }
        writeLine("stdout",
                  "DEBUG RegisterData: EPIC PID lookup for *path: *epic.");
    }
    else{
        *epic = isregistered(*path, *objType);
	if(*epic == ""){
            writeLine("stdout","DEBUG RegisterData: Create EPIC PID for *path.");
	    register(*path, *proto, *objType);
	}
	*epic = isregistered(*path, *objType);
	writeLine("stdout",
                  "DEBUG RegisterData: EPIC PID lookup for *path: *epic.");
    }
}

#Helper functions
register(*path, *endpoint, *objType){

    msiExecCmd("uuid","","","","",*createUuid);
    msiGetStdoutInExecCmdOut(*createUuid,*uuid);
    msiCreateEpicPID(*uuid, "*endpoint*path", *handle, *code);
    addPIDProfile(*handle, *path);
    if(*code == "201"){
        writeLine("stdout", "DEBUG RegisterData: Created *handle for *path");
        msiAddKeyVal(*Keyval,"EPIC_PID", *handle);
        msiAssociateKeyValuePairsToObj(*Keyval,*path,*objType);
    }
    else{
        writeLine("stdout",
                 "ERROR RegisterData: Failed to create EPIC PID for *path: *code");
    }
}

isregistered(*path, *objType){
    # Checks if EPIC PID for *path is known in the iRODS metadata, key=EPIC_PID.
    # Returns the EPIC PID or "".
    *epic = "";
    if(*objType == "-d"){
        msiSplitPath(*path, *coll, *obj);
        foreach(*row in SELECT META_DATA_ATTR_VALUE where META_DATA_ATTR_NAME = 'EPIC_PID' and COLL_NAME = '*coll' and DATA_NAME = '*obj'){ 
            *epic = *row.META_DATA_ATTR_VALUE;
	}
    }
    if(*objType == '-c'){
        foreach(*row in SELECT META_COLL_ATTR_VALUE where META_COLL_ATTR_NAME == "EPIC_PID" and COLL_NAME == "*path"){
            *epic = *row.META_COLL_ATTR_VALUE;
	}
    }
    *epic;
} 

addPIDProfile(*handle, *path){
    msiUpdateEpicPID(*handle, "IRODS_SERVER", "scomp1448.wurnet.nl", *out);
    msiUpdateEpicPID(*handle, "IRODS_ZONE", "bobZone", *out);
    msiUpdateEpicPID(*handle, "IRODS_PORT", "1247", *out);
    msiUpdateEpicPID(*handle, "IRODS_PATH", *path, *out);
}

input *path='/<zone>/home/<user>/<obj or coll>',*recursive='false',*proto='irods:'
output ruleExecOut
