deregisterData{
    # Removes a PID for *path and the iRODS metadata key=EPIC_PID, value=Handle
    # If the input path is a collection and the recursive flag is set to true 
    # all members of the collection will be checked.
    #
    # *path: full path do object or collection
    # *recursive: apply to all memebers of a collection, ignored if path is an object
    
    msiGetObjType(*path, *objType);
    if(*objType == "-c" && *recursive == "true"){
        writeLine("stdout","DEBUG DeregisterData: Remove EPIC PID for *path and all its members.");
        # deregister all data objects in the collection
        foreach(*row in SELECT COLL_NAME, DATA_NAME where COLL_NAME like "*path"){
	    *coll = *row.COLL_NAME;
            *obj = *row.DATA_NAME;
            *epic = isregistered("*coll/*obj", "-d");
	    if(*epic != ""){ deregister("*coll/*obj", *epic, "-d"); }
	}
	# deregister all data objects in subcollections
        foreach(*row in SELECT COLL_NAME, DATA_NAME where COLL_NAME like "*path/%"){
            *coll = *row.COLL_NAME;
            *obj = *row.DATA_NAME;
            *epic = isregistered("*coll/*obj", "-d");
	    if(*epic != ""){ deregister("*coll/*obj", *epic, "-d"); }
        }
	# deregister all subcollections
        foreach(*row in SELECT COLL_NAME where COLL_NAME like "*path/%"){
            *coll = *row.COLL_NAME;
            *epic = isregistered(*coll, "-c");
	    if(*epic != ""){ deregister("*coll", *epic, "-c"); }
	}
	# deregister the collection itself
        *epic = isregistered(*path, *objType);
	if(*epic != ""){ deregister("*coll", *epic, "-c"); }
    }
    else{
        writeLine("stdout","DEBUG DeregisterData: Remove EPIC PID for *path.");
        *epic = isregistered(*path, *objType);
	if(*epic != ""){ deregister(*path, *epic, *objType); }
    }
}

#Helper functions

deregister(*path, *epic, *objType){
   msiString2KeyValPair("EPIC_PID=*epic",*Keyval);
   msiRemoveKeyValuePairsFromObj(*Keyval, *path, *objType);
   msiDeleteEpicPID(*epic, *out);
   writeLine("stdout", "DEBUG DeregisterData: *epic remove status *out");
}

isregistered(*path, *objType){
    # Checks if EPIC PID for *path is known in the iRODS metadata, key=EPIC_PID.
    # Returns the EPIC PID or "".
    *epic = "";
    if(*objType == "-d"){
        msiSplitPath(*path, *coll, *obj);
        foreach(*row in SELECT META_DATA_ATTR_VALUE where META_DATA_ATTR_NAME = 'EPIC_PID' and COLL_NAME = '*coll' and DATA_NAME = '*obj'){ 
            *epic = *row.META_DATA_ATTR_VALUE;
	}
    }
    if(*objType == '-c'){
        foreach(*row in SELECT META_COLL_ATTR_VALUE where META_COLL_ATTR_NAME == "EPIC_PID" and COLL_NAME == "*path"){
            *epic = *row.META_COLL_ATTR_VALUE;
	}
    }
    *epic;
} 

input *path='/<ZONE>/home/<user>/<obj or coll>',*recursive='false'
output ruleExecOut
