updateEpicUrl{
    # Moves registered data to a new subtree and updates the PID 
    # If the input path is a collection and the recursive flag is set to true 
    # all members of the collection will receive an individual EPIC PID.
    #
    # *path: full path do object or collection
    # *recursive: apply to all memebers of a collection, if=gnored if path is an object
    
    msiGetObjType(*path, *objType);
    *epic = isregistered(*path, *objType);
    if(*epic != ""){
        if(*objType == "-c" && *recursive == "true"){
            writeLine("stdout","DEBUG RegisterData: Create EPIC PID for *path and all its members.");
            foreach(*row in SELECT COLL_NAME, DATA_NAME where COLL_NAME like *path){
		*coll = *row.COLL_NAME;
	        *obj = *row.DATA_NAME;
	        *epic = isregistered("*coll/*obj", "-d");
		if(*epic != ""){updatePIDprofile(*epic, "*coll/*obj", *proto)}
	    }
	    foreach(*row in SELECT COLL_NAME, DATA_NAME where COLL_NAME like "*path/%"){
                *coll = *row.COLL_NAME;
                *obj = *row.DATA_NAME;
                *epic = isregistered("*coll/*obj", "-d");
                if(*epic != ""){updatePIDprofile(*epic, "*coll/*obj", *proto)}
            }
	    foreach(*row in SELECT COLL_NAME where COLL_NAME like "*path/%"){
                *coll = *row.COLL_NAME;
                *epic = isregistered("*coll", "-c");
                if(*epic != ""){updatePIDprofile(*epic, "*coll", *proto)}
            }
            if(*epic != ""){updatePIDprofile(*epic, "*path", *proto)}
    }
        else{
	    updatePIDprofile(*epic,*path,*proto);
        }
    }
}

#Helper functions
updatePIDprofile(*epic, *path, *proto){
    msiUpdateEpicPID(*epic, "URL", "*proto/*path", *out);
    msiUpdateEpicPID(*epic, "IRODS_PATH", *path, *out);
}

isregistered(*path, *objType){
    # Checks if EPIC PID for *path is known in the iRODS metadata, key=EPIC_PID.
    # Returns the EPIC PID or "".
    *epic = "";
    if(*objType == "-d"){
        msiSplitPath(*path, *coll, *obj);
        foreach(*row in SELECT META_DATA_ATTR_VALUE where META_DATA_ATTR_NAME = 'EPIC_PID' and COLL_NAME = '*coll' and DATA_NAME = '*obj'){ 
            *epic = *row.META_DATA_ATTR_VALUE;
	}
    }
    if(*objType == '-c'){
        foreach(*row in SELECT META_COLL_ATTR_VALUE where META_COLL_ATTR_NAME == "EPIC_PID" and COLL_NAME == "*path"){
            *epic = *row.META_COLL_ATTR_VALUE;
	}
    }
    *epic;
} 

input *path='/<zone>/home/<user>/<obj or coll>',*proto='irods:',*recursive='false'
output ruleExecOut
