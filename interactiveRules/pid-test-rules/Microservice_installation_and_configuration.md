# Microservice installation and configuration

# PID microservices and rules

## Synopsis

This document describes how to install the EPIC PID microservices. All PIDs are resolvable through:

```
http://hdl.handle.net/
```

e.g.:

```
http://hdl.handle.net/<prefix>/<suffix>
http://hdl.handle.net/<prefix>/<suffix>?noredirect
```

## Prerequisites

### Install the PID micro services (server admin rights needed) :

```
git clone https://git.wur.nl/rdm-infrastructure/irods-microservices.git
sudo apt-get install irods-dev irods-externals-cmake3.11.4 irods-externals-clang6.0 
sudo apt-get make libcurl4-openssl-dev libboost-dev libjansson-dev
```



```
cd irods-microservices
export PATH=/opt/irods-externals/cmake3.11.4-0/bin:$PATH
cmake .
make
make install
```

### Configure the microservices with the Handle prefix and credentials

- As iRODS service account do:

  ```
  mkdir /var/lib/irods/.credentials_store
  ```

- Place the Handle certificates with which you authenticate with the Handle server in that folder:

  ```
  ls /var/lib/irods/.credentials_store
  epic_cert.pem  epic_key.pem
  ```

- Place the file `store_config.json`in that folder with the following content (Adjust where necessary!):

  ```
  {
      "epic_url": "https://epic5.storage.surfsara.nl:8003/api/handles",
      "epic_handle_prefix": "21.T12995",
      "epic_key" : "/var/lib/irods/.credentials_store/epic_key.pem",
      "epic_certificate" : "/var/lib/irods/.credentials_store/epic_cert.pem"
  }
  ```

### Configure UUID module for  iRODS

The PID suffix is by bets practice a UUID. 

- Install uuid

  ```
  sudo apt-get install uuid
  ```

- Create an executable script for iRODS that calls `uuid` and returns the uuid as string:

  ```
  #!/bin/sh
  # echo "execCmdRule: "$execCmdRule
  UUID=`uuid`
  echo -n $UUID
  ```

  Save the script in /var/lib/irods/msiExecCmd_bin/uuid and make it executable:

  ```
  chmod +x /var/lib/irods/msiExecCmd_bin/uuid
  ```

  
